/*

    Quick and Dirty Game Development Framework (QDGDF)

    Copyright (C) 2004/2011 Angel Ortega <angel@triptico.com>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

/* video driver skeleton */

#include "config.h"

#ifdef CONFOPT_SKELETON

#include "qdgdf_video.h"


/** data **/

/** code **/

static void _qdgdfv_set_palette(void)
{
}


static void _qdgdfv_dump_virtual_screen(void)
{
}


void _qdgdfv_input_poll(void)
{
}


static int _qdgdfv_timer(int reset)
{
    return 0;
}


static int _qdgdfv_opengl(int onoff)
{
    return 0;
}


static int _qdgdfv_startup(void)
{
    return 1;
}


static void _qdgdfv_shutdown(void)
{
}


/* driver information */

static struct _qdgdfv_driver drv = {
    "skeleton",
    _qdgdfv_set_palette,
    _qdgdfv_dump_virtual_screen,
    _qdgdfv_input_poll,
    _qdgdfv_timer,
    _qdgdfv_opengl,
    _qdgdfv_startup,
    _qdgdfv_shutdown
};


struct _qdgdfv_driver *skeleton_drv_detect(void)
/* detection function */
{
    if (_qdgdfv_startup())
        return &drv;

    return NULL;
}


#endif                          /* CONFOPT_SKELETON */
