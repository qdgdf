/*

    Quick and Dirty Game Development Framework (QDGDF)

    Copyright (C) 2001/2011 Angel Ortega <angel@triptico.com>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://triptico.com

    Usage:
    qdgdf_i [X size] [Y size] [scale] [full screen]

*/

#include "config.h"
#include "qdgdf_video.h"
#include "qdgdf_audio.h"

#include <string.h>

char *info[100];

void build_info(void)
{
    int y = 0;

    info[y++] = strdup(qdgdfv_sprintf("QDGDF %s", _qdgdfv_version));
    info[y++] = strdup(qdgdfv_sprintf("%d x %d (scale %d)",
                                      _qdgdfv_screen_x_size,
                                      _qdgdfv_screen_y_size,
                                      _qdgdfv_scale));
    info[y++] = strdup(qdgdfv_sprintf("full screen: %d",
                                      _qdgdfv_full_screen));
    info[y++] = strdup(qdgdfv_sprintf("window position: %d x %d",
                                    _qdgdfv_window_x,
                                    _qdgdfv_window_y));
    info[y++] = strdup(qdgdfv_sprintf("pixel size: %d", _qdgdfv_pixel_size));
    info[y++] = strdup(qdgdfv_sprintf("home dir: %s", qdgdfv_home_dir()));
    info[y++] = strdup(qdgdfv_sprintf("app dir: %s", qdgdfv_app_dir()));
    info[y++] = strdup(qdgdfv_sprintf("sound: %d (%d bit)",
                                      _qdgdfa_sound,
                                      _qdgdfa_16_bit ? 16 : 8));

#ifdef CONFOPT_OPENGL
    info[y++] = "OpenGL support enabled";
#else
    info[y++] = "OpenGL support disabled";
#endif
}


void show_info(void)
{
    int c = qdgdfv_seek_color(255, 255, 255);
    int y;

    qdgdfv_clear_virtual_screen();

    for (y = 0; info[y] != NULL; y++)
        qdgdfv_font_print(1, y * _qdgdfv_font_height, info[y], c);

    qdgdfv_font_print(1, y * _qdgdfv_font_height,
            qdgdfv_sprintf("%d %c", _qdgdfv_key_alnum, _qdgdfv_alnum),
            c);

    qdgdfv_font_print(-1, -1, "Press ESCAPE", c);

    qdgdfv_dump_virtual_screen();
}


void show_opengl(void)
{
#ifdef CONFOPT_OPENGL
    static double theta = 0.0;

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glPushMatrix();
    glRotatef(theta, 0.0f, 0.0f, 1.0f);
    glBegin(GL_TRIANGLES);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex2f(0.0f, 1.0f);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex2f(0.87f, -0.5f);
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex2f(-0.87f, -0.5f);
    glEnd();

    glPopMatrix();

    theta += 1.0;

    qdgdfv_dump_virtual_screen();
#endif
}


int main(int argc, char *argv[])
{
    int n = 1;

    strcpy(_qdgdfv_window_title, "QDGDF Information");

    if (argc > n)
        _qdgdfv_screen_x_size = atoi(argv[n++]);
    if (argc > n)
        _qdgdfv_screen_y_size = atoi(argv[n++]);
    if (argc > n)
        _qdgdfv_scale = atoi(argv[n++]);
    if (argc > n)
        _qdgdfv_full_screen = atoi(argv[n++]);

    strcpy(_qdgdfv_logger_file, "qdgdf.log");
    _qdgdfv_use_logger = 1;

    qdgdfa_startup();
    qdgdfv_startup();

    build_info();

    do {
        show_info();
        qdgdfv_input_poll();
    } while (!_qdgdfv_key_escape);

    while (_qdgdfv_key_escape)
        qdgdfv_input_poll();

    if (qdgdfv_opengl(1)) {
        do {
            show_opengl();
            qdgdfv_input_poll();
        } while (!_qdgdfv_key_escape);
    }

    qdgdfv_shutdown();
    qdgdfa_shutdown();

    return 0;
}
