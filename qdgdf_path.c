/*

    qdgdfv_path.c

    Copyright (C) 2005/2006 Angel Ortega <angel@triptico.com>

    Path-related functions.

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

#ifdef CONFOPT_UNISTD_H
#include <unistd.h>
#endif

#ifdef CONFOPT_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef CONFOPT_PWD_H
#include <pwd.h>
#endif

static char *_path_find(const char *path, const char *file)
/* finds file in path; returns full path if found, NULL instead */
{
    char *cpath;
    char *ret = NULL;
    char *ptr1;

    /* no path? nothing to do */
    if (path == NULL && *path == '\0')
        return NULL;

    cpath = strdup(path);
    ptr1 = cpath;

    for (;;) {
        char tmp[150];
        char *ptr2;

        if ((ptr2 = strchr(ptr1, ';')) != NULL)
            *ptr2 = '\0';

        snprintf(tmp, sizeof(tmp) - 1, "%s/%s", ptr1, file);
        tmp[sizeof(tmp) - 1] = '\0';

        if (access(tmp, R_OK) == 0) {
            ret = strdup(tmp);
            break;
        }

        if (ptr2 == NULL)
            break;

        ptr1 = ptr2 + 1;
    }

    free(cpath);

    return ret;
}


static FILE *_path_fopen(const char *path, const char *file, char *mode)
/* file opening function with optional seek path */
{
    FILE *f = NULL;
    char *ptr;

    if ((ptr = _path_find(path, file)) != NULL) {
        f = fopen(ptr, mode);
        free(ptr);
    }

    if (f == NULL)
        f = fopen(file, mode);

    return f;
}


#if defined(CONFOPT_DDRAW) || defined(CONFOPT_DSOUND)
#include <shlobj.h>
#endif

static char *_home_dir(void)
/* returns a writable home directory */
{
    char *ptr;
    static char tmp[2048] = "";

#ifdef CONFOPT_WIN32

    LPITEMIDLIST pidl;

    /* get the 'My Documents' folder */
    SHGetSpecialFolderLocation(NULL, CSIDL_PERSONAL, &pidl);
    SHGetPathFromIDList(pidl, tmp);
    strcat(tmp, "\\");

#endif

#if defined(CONFOPT_PWD_H) && defined(CONFOPT_SYS_TYPES_H) && defined(CONFOPT_UNISTD_H)

    struct passwd *p;

    /* get home dir from /etc/passwd entry */
    if (tmp[0] == '\0' && (p = getpwuid(getpid())) != NULL) {
        strcpy(tmp, p->pw_dir);
        strcat(tmp, "/");
    }

#endif

    /* still none? try the ENV variable $HOME */
    if (tmp[0] == '\0' && (ptr = getenv("HOME")) != NULL) {
        strcpy(tmp, ptr);
        strcat(tmp, "/");
    }

    return tmp;
}


static char *_app_dir(void)
{
    static char tmp[1048] = "";

#ifdef CONFOPT_WIN32

    HKEY hkey;
    LPITEMIDLIST pidl;

    /* get the 'Program Files' folder (can fail) */
    tmp[0] = '\0';
    if (SHGetSpecialFolderLocation(NULL, CSIDL_PROGRAM_FILES, &pidl) ==
        S_OK)
        SHGetPathFromIDList(pidl, tmp);

    /* if it's still empty, get from the registry */
    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,
                     "SOFTWARE\\Microsoft\\Windows\\CurrentVersion",
                     0, KEY_QUERY_VALUE, &hkey) == ERROR_SUCCESS) {
        int n = sizeof(tmp);

        if (RegQueryValueEx(hkey, "ProgramFilesDir",
                            NULL, NULL, tmp,
                            (LPDWORD) & n) != ERROR_SUCCESS)
            tmp[0] = '\0';
    }

    if (tmp[0] != '\0')
        strcat(tmp, "\\");
#endif

    /* still none? get the configured directory */
    if (tmp[0] == '\0')
        strcpy(tmp, CONFOPT_PREFIX "/share/");

    return tmp;
}
